import smtplib

def send_mail(your_email:str, your_password:str, subject: str, body: str):
    """
    Sends mail for the given price.
    """
    domain = your_email.split("@")[-1]

    if domain == "yahoo.com":
        server = smtplib.SMTP('smtp.mail.yahoo.com', 587)
    elif domain == "gmail.com":
        server = smtplib.SMTP('smtp.gmail.com', 587)
    else:
        return "Invalid email"

    server.ehlo()
    server.starttls()
    server.login(your_email, your_password)
    server.sendmail(your_email,
                        your_email, f"Subject: {subject}\n\n{body}")
    server.quit()