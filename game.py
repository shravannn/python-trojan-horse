from random import randint

def game():
    print("Welcome to the Guess the Number game!")
    print("The number is between 1 and 1000. Guess it.\n")

    n = randint(1, 1000)
    while True:
        try:
            guess = int(input("Guess the number: "))

            if guess > n:
                print("Decrease the number!")

            elif guess < n:
                print("Increase the number!")

            else:
                print("You won!")
                break

        except Exception as e:
            print(e)


if __name__ == "__main__":
    game()